package com.training.springmvcdemo.dao;

import com.training.springmvcdemo.beans.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;


public class UserDao {
	
	@Autowired
	private JdbcTemplate template;
	
	public User checkUser(String name, String pwd) {
		User user = null;
        try {
            String sql = "select * from users where userEmail = ? and userPwd = ?";
            user  = template.queryForObject(
                    sql, new UserMapper(), name, pwd);
          } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
          } catch (IncorrectResultSizeDataAccessException e) {
            e.printStackTrace();
          }
		return user;
	}
}
