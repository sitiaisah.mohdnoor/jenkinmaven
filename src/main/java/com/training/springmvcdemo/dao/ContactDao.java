package com.training.springmvcdemo.dao;

import java.util.List;

import com.training.springmvcdemo.beans.Contact;
import com.training.springmvcdemo.exception.ContactException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;


public class ContactDao {
	
//	private JdbcTemplate template = ConnectionUtil.getConnection();
	
	@Autowired
	private JdbcTemplate template;

	public boolean addContact(String name, String phone, int userId) {
		boolean success = false;
         try {
             String insertQ = "insert into contact(contactName, contactPhone, userId) values(?,?,?);";
             template.update(insertQ, name, phone, userId);
             success = true;
           } catch (DataAccessException ex) {
             ex.printStackTrace();
           }
		return success;
	}
	
  public Contact getContact(int contactId){
		Contact contact = null;
        try {
            String sql = "Select * from contact where contactId = ?";
            contact = template.queryForObject(sql, new ContactMapper(), contactId);
            System.out.println("detail " + contact);
          } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
          } catch (IncorrectResultSizeDataAccessException e) {
            e.printStackTrace();
          }
		return contact;
	}

	public List<Contact> getAllContact(int userId){
		List<Contact> list = null;
        try {
            String sql = "Select * from contact where userId = ?";
            list = template.query(sql, new ContactMapper(), userId);
          } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
          } catch (IncorrectResultSizeDataAccessException e) {
            e.printStackTrace();
          }
		return list;
	}
	
	public boolean updateContact(String newName, String newNo, int contactId) {
		boolean success = false;
         try {
             String sql = "update contact set contactName = ?, contactPhone = ? where contactId = ? ";
             template.update(sql, newName, newNo, contactId);
             success = true;
           } catch (DataAccessException ex) {
             ex.printStackTrace();
           }
		return success;
	}
	
	public boolean deleteContact(int contactId) throws ContactException{
		boolean success = false;
         try {
             String sql = "delete from contact where contactId = ? ";
             int count = template.update(sql, contactId);
             if(count > 0) {success = true;}
             else {throw new ContactException("id does not exist");}
           } catch (DataAccessException ex) {
             ex.printStackTrace();
           }
		return success;
	}

}
