package com.training.springmvcdemo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.training.springmvcdemo.beans.User;

import org.springframework.jdbc.core.RowMapper;

public class UserMapper implements RowMapper<User> {

	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		user.setId(rs.getInt("userId"));
		user.setName(rs.getString("userName"));
		user.setEmail(rs.getString("userEmail"));  
		user.setPwd(rs.getString("userPwd"));   
        return user;
	}

}
