package com.training.springmvcdemo.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.training.springmvcdemo.beans.Contact;

import org.springframework.jdbc.core.RowMapper;



public class ContactMapper implements RowMapper<Contact> {

	public Contact mapRow(ResultSet rs, int rowNum) throws SQLException {
		Contact contact = new Contact();
		contact.setContactId(rs.getInt("contactId"));
		contact.setContactName(rs.getString("contactName"));
		contact.setContactPhone(rs.getString("contactPhone"));       
        return contact;
	}

}
