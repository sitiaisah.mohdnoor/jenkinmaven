package com.training.springmvcdemo.config;

import com.training.springmvcdemo.beans.Contact;
import com.training.springmvcdemo.beans.User;
import com.training.springmvcdemo.dao.ContactDao;
import com.training.springmvcdemo.dao.UserDao;
import com.training.springmvcdemo.service.ContactService;
import com.training.springmvcdemo.service.IContactService;
import com.training.springmvcdemo.service.IUserService;
import com.training.springmvcdemo.service.UserService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


/**
 * declares a class provides one or more @Bean methods
 * may be used by the spring container to generate the bean definitions 
 * and serve requests for those beans at runtime
 */

@Configuration
//@ComponentScan("com.training.contactmanager.beans")
@ComponentScan
public class AppConfig {
	/**
	 *should use default constructor instead of hardcoded value
	 *ask user input and call AppConfig to create object instance
	 *
	 *create service interface and service impl as well
	 *call it in psvm via AppConfig
	 *
	 *evrything thats being defined here, u can use @ autowired to call it elsewhere
	 */
	private static String USERNAME = "root";
	private static String PASSWORD = "admin";
	private static String URL = "jdbc:mysql://localhost:3306/contactmanager";
	
	@Bean
	public IUserService userService() {
		return new UserService();
	}

	@Bean
	public IContactService contactService() {
		return new ContactService();
	
	}
	
	@Bean
	public UserDao userDao() {
		return new UserDao();
	}

	@Bean
	public ContactDao contactDao() {
		return new ContactDao();
	
	}
	
	@Bean
	public DriverManagerDataSource dataSource() {
        DriverManagerDataSource dataSrc = new DriverManagerDataSource();
		dataSrc.setDriverClassName("com.mysql.jdbc.Driver"); //not neded in eclipse
        dataSrc.setUrl(URL);
        dataSrc.setUsername(USERNAME);
        dataSrc.setPassword(PASSWORD);
        return dataSrc;
	}
	
	@Bean
	public JdbcTemplate template() {
		JdbcTemplate template = new JdbcTemplate();
		template.setDataSource(dataSource());
		return template;
	}
	
	@Bean
	@Scope(value="prototype")
	public User getUser() {
		return new User();
	}
	
	@Bean
	@Scope(value="prototype")
	public Contact getContact() {
		return new Contact();
	}

	
}

