package com.training.springmvcdemo.controller;

import com.training.springmvcdemo.beans.User;
import com.training.springmvcdemo.service.IUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

//Every controller must be annotated by @Controller.
//Controller has two methods. First one will serve the login page request
//Second one will implement login logic and redirect the login success and failure message. 
//Every method which is serving request must be annotated by @RequestMapping.
@Controller
public class LoginController {

	@Autowired
	IUserService userService;

	@GetMapping("login")
	public String login(){
		return "login";
	}

	/**
	 * 
	 *  use @RequestParam for sending parameter via api url (?param=x)
	 *  use @PathVariable for sending param via url(/param), when param changes dynamically
	 *  use @RequestBody for sending via http body, used with JSON, available later with spring boot
	 */
	
	@RequestMapping("auth-login")
	public ModelAndView loginAuth(@RequestParam("email") String email, @RequestParam("pwd") String pwd, ModelMap model) {
		
		User user = userService.checkUser(email, pwd);
		System.out.println("current user:  " + user);

		ModelAndView mav = null;
		if(user != null){
			mav = new ModelAndView("redirect:/display-contact");
			mav.addObject("userId", user.getId());
		}else{

			//mav.addObject("message", "Username or password is wrong.");
		}
		return mav;
	}
}
