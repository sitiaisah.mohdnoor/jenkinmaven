package com.training.springmvcdemo.controller;

import java.util.List;

import com.training.springmvcdemo.beans.Contact;
import com.training.springmvcdemo.service.IContactService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ContactController {
    
	@Autowired
	IContactService ContactService;

	@RequestMapping(value="contact-form/{userId}", method = RequestMethod.GET)
	public ModelAndView getContactForm(@PathVariable("userId") int userId){
		System.out.println("form userId: " + userId);
		// return "contact-form";
		ModelAndView mv=new ModelAndView();
		mv.setViewName("contact-form");
		mv.addObject("userId", userId);
		mv.addObject("customUrl", "/springmvcdemo/add-contact/"+userId);
		return mv;
	}

	@GetMapping("update-form/{contactId}")
	public ModelAndView getUpdateForm(@PathVariable("contactId") int contactId){
		System.out.println("update form contactId: " + contactId);
		ModelAndView mv=new ModelAndView();
		mv.setViewName("contact-form");
		mv.addObject("contactId", contactId);
		mv.addObject("customUrl", "/springmvcdemo/update-contact/"+contactId);
		return mv;
	}

	@RequestMapping(value = "/contact-detail/{contactId}", method = RequestMethod.GET)
	public ModelAndView getDetail(@PathVariable("contactId") int contactId) {
		System.out.println("contactId " + contactId);
		Contact contact = ContactService.getContact(contactId);
		ModelAndView mv=new ModelAndView();
		mv.setViewName("contact-detail");
		mv.addObject("message","hai");
		mv.addObject("detail", contact);
		return mv;		
	}

	// @PostMapping("contact-form/add-contact/{userId}")
	@PostMapping("add-contact/{userId}")
	public ModelAndView addContact(@PathVariable("userId") int userId, @RequestParam("name") String name, @RequestParam("phone") String phone) {	
		System.out.println("form:  " + name + phone + userId);
		boolean success = ContactService.addContact(name, phone, userId);
		ModelAndView mv = null;
		System.out.println("add success " + success);
		if(success){
			mv = new ModelAndView("redirect:/display-contact?userId="+userId);
		}else{

		}
		return mv;
	}

	// @PostMapping("update-form/update-contact/{contactId}")
	@PostMapping("update-contact/{contactId}")
	public ModelAndView updateContact(@PathVariable("contactId") int contactId, @RequestParam("name") String name, @RequestParam("phone") String phone) {	
		System.out.println("update:  " + name + phone + contactId);
		boolean success = ContactService.updateContact(name, phone, contactId);
		System.out.println("update success " + success);
		ModelAndView mv = null;
		if(success){
			// mv = new ModelAndView("redirect:/display-contact");
		}else{

		}
		return mv;
	}

	@GetMapping("/contact-delete/{contactId}")
	public ModelAndView deleteContact(@PathVariable("contactId") int contactId){
		System.out.println("delete contactId: " + contactId);
		boolean success = ContactService.deleteContact(contactId);
		System.out.println("delete success " + success);
		ModelAndView mv = null;
		if(success){
			// mv = new ModelAndView("redirect:/display-contact");
		}else{

		}
		return mv;

	}

    @RequestMapping(value="display-contact", method = RequestMethod.GET)
	public ModelAndView getContact(@RequestParam int userId){
		System.out.println("userId: " + userId);
		//retrieve from db
		List<Contact> list = ContactService.getAllContact(userId);
		System.out.println(list);
		ModelAndView mv=new ModelAndView();
		mv.setViewName("display-all");
		mv.addObject("contacts", list);
		mv.addObject("userId", userId);
		return mv;
	}
}

