package com.training.springmvcdemo.beans;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class User {
	
	private int id;
	private String name;
	private String email;
	private String pwd;
	private Contact contact;
	private List<Contact> contactList;
	private Map<String,Contact> contactMap;
	private Set<Contact> contactSet;
	
	public User() {

	}
	
	public User(String name, String email, String pwd) {
		this.name = name;
		this.email = email;
		this.pwd = pwd;
	}

	public User(String name, String email, String pwd, Contact contact) {
		this.name = name;
		this.email = email;
		this.pwd = pwd;
		this.contact = contact;
	}
	

	public User(String name, String email, String pwd, List<Contact> contactList) {
		this.name = name;
		this.email = email;
		this.pwd = pwd;
		this.contactList = contactList;
	}
	
	public User(String name, String email, String pwd, Map<String, Contact> contactMap) {
		super();
		this.name = name;
		this.email = email;
		this.pwd = pwd;
		this.contactMap = contactMap;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public List<Contact> getContactList() {
		return contactList;
	}

	public void setContactList(List<Contact> contactList) {
		this.contactList = contactList;
	}

	public Map<String, Contact> getContactMap() {
		return contactMap;
	}

	public void setContactMap(Map<String, Contact> contactMap) {
		this.contactMap = contactMap;
	}
	
	public Set<Contact> getContactSet() {
		return contactSet;
	}

	public void setContactSet(Set<Contact> contactSet) {
		this.contactSet = contactSet;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", email=" + email + ", pwd=" + pwd + ", contact=" + contact + ", contactList="
				+ contactList + ", contactMap=" + contactMap + ", contactSet=" + contactSet + "]";
	}
		
	
}
