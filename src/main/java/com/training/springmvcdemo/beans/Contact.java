package com.training.springmvcdemo.beans;

public class Contact {
	
	private int contactId;
	private String contactName;
	private String contactPhone;
	
	
	public Contact() {

	}

	public Contact(String contactName, String contactPhone) {
		this.contactName = contactName;
		this.contactPhone = contactPhone;
	}
	
	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	@Override
	public String toString() {
		return "Contact [contactName=" + contactName + ", contactPhone=" + contactPhone + "]";
	}
	
	
}
