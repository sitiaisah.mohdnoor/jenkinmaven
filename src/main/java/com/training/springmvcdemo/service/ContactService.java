package com.training.springmvcdemo.service;

import java.util.List;

import com.training.springmvcdemo.beans.Contact;
import com.training.springmvcdemo.dao.ContactDao;
import com.training.springmvcdemo.exception.ContactException;

import org.springframework.beans.factory.annotation.Autowired;


public class ContactService implements IContactService{
	
//	ContactDao dao = new ContactDao();
	@Autowired
	ContactDao dao;
	
	public boolean addContact(String name, String phone, int userId) {
		return dao.addContact( name,  phone, userId);
	}
	
	public List<Contact> getAllContact(int userId){
		return dao.getAllContact(userId); 
	}

	public boolean updateContact(String newName, String newNo, int contactId) {
		// TODO Auto-generated method stub
		return dao.updateContact( newName, newNo,  contactId);
	}

	public boolean deleteContact(int contactId) {
		boolean success = false;
		try {
			success = dao.deleteContact(contactId);
		} catch (ContactException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

	@Override
	public Contact getContact(int contactId) {
		// TODO Auto-generated method stub
		return dao.getContact(contactId);
	}
}
