package com.training.springmvcdemo.service;

import java.util.List;

import com.training.springmvcdemo.beans.Contact;

public interface IContactService {
	
	public boolean addContact(String name, String phone, int userId);
	
	public List<Contact> getAllContact(int userId);
	
	public boolean updateContact(String newName, String newPhone, int contactId);
	
	public boolean deleteContact(int contactId);

	public Contact getContact(int contactId);
}
