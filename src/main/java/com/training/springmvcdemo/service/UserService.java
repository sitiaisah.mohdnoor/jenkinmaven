package com.training.springmvcdemo.service;

import com.training.springmvcdemo.beans.User;
import com.training.springmvcdemo.dao.UserDao;

import org.springframework.beans.factory.annotation.Autowired;

public class UserService implements IUserService{
	
//	UserDao dao = new UserDao();
	@Autowired
	UserDao dao;
	
	public User checkUser(String name, String pwd) {
		return dao.checkUser( name,  pwd);
		
	}
}
