package com.training.springmvcdemo.service;

import com.training.springmvcdemo.beans.User;

public interface IUserService {

	public User checkUser(String name, String pwd);
}
