package com.training.springmvcdemo.util;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class ConnectionUtil {

	
	private static JdbcTemplate template;
	private static String USERNAME = "root";
	private static String PASSWORD = "admin";
	private static String URL = "jdbc:mysql://localhost:3306/contactmanager";
	
	private ConnectionUtil(){
		
	}
	
    public static JdbcTemplate getConnection(){    	
		if(template == null) {
	        template = new JdbcTemplate(getDatabase());
	
		}
    	return template;
    }
    
    public static DataSource getDatabase(){
        //2.inform spring about db
        DriverManagerDataSource dataSrc = new DriverManagerDataSource();
        //3.set driver class
        dataSrc.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSrc.setUrl(URL);
        dataSrc.setUsername(USERNAME);
        dataSrc.setPassword(PASSWORD);
        return dataSrc;
    }
}
