<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  <title>Login</title>
</head>
<%-- 
  modelAttribute - key which specifies a name of the model object that backs this form. 
                 - will correspond to the @ModelAttribute later on in the controller
  path - correspond to a getter/setter of the model attribute
       - When the page is loaded, it calls the getter of each field bound to an input field
       - When the form is submitted, the setters are called to save the values of the form to the object.
--%>
	<body class="pt-5">
     <div class="container mt-5 mb-5">
      <form:form action="auth-login" method="post">
        <div class="form-group">
          <label for="email">Email address</label>
          <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
        </div>
        <div class="form-group">
          <label for="pwd">Password</label>
          <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Password">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form:form>
	  </div>    
	</body>
</html> 