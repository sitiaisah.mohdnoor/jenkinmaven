<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  <title>Form</title>
</head>
	<body class="pt-5">
     <div class="container mt-5 mb-5">
      <c:out value="${customUrl}"></c:out>
      <form:form action="${customUrl}"  method="post">
        <div class="form-group">
          <label for="name">Name</label>
          <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
        </div>
        <div class="form-group">
          <label for="phone">Phone</label>
          <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone No.">
        </div>
        <div class="form-group">
          <label for="userId">userId</label>
          <input disabled type="text" class="form-control" id="userId" name="userId" value="${userId}">
        </div>
         <div class="form-group">
          <label for="contactId">contactId</label>
          <input disabled type="text" class="form-control" id="contactId" name="contactId" value="${contactId}">
        </div>
        <c:if test = "${userId != null}">
   	      <button type="submit" class="btn btn-primary">Add</button>
           <%-- <c:url var="post_url"  value="/add-contact/${userId}" />
           <c:out value="${post_url}"></c:out> --%>
           <%-- <a class="btn btn-primary"href=' <c:url value="/add-contact/${userId}"></c:url> ' role="button">Add</a> --%>
		   	</c:if>	
        <c:if test = "${contactId != null}">
   	      <button type="submit" class="btn btn-primary">Update</button>
           <%-- <c:url var="post_url"  value="/update-contact/${contactId}" />
           <c:out value="${post_url}"></c:out> --%>
          <%-- <a class="btn btn-primary" href=' <c:url value="/update-contact/${contactId}"></c:url> ' role="button">Update</a> --%>
         </c:if>	
      </form:form>
	  </div>    
	</body>
</html> 