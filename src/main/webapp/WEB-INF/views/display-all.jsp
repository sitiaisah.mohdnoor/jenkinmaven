<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ page import="java.util.*, com.training.springmvcdemo.beans.*" %>  
<!DOCTYPE html>
<html>
    <head>
    	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
    </head>
	<body class="pt-5">
     <div class="container mt-5 mb-5">
    	<h3>All Contacts</h3>
		<c:set var="contacts" value="${contacts}" />
        <table class="table">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Phone</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="con" items= "${contacts}" varStatus="i">
                <%-- <p >${con.getContactName()}</p>    --%>
                    <tr>
                        <th scope="row">${con.getContactId()}</th>
                        <td>${con.getContactName()}</td>
                        <td>${con.getContactPhone()}</td>
                        <td> 
                            <a href=<c:url value="contact-detail/${con.getContactId()}"></c:url>
                            >
                                view
                            </a>
                        </td>
                        <td> 
                            <a href=<c:url value="update-form/${con.getContactId()}"></c:url>
                            >
                                Update
                            </a>
                        </td>
                        <td> 
                            <a href=<c:url value="contact-delete/${con.getContactId()}"></c:url>
                            >
                                Delete
                            </a>
                        </td>
                    </tr>
            </c:forEach>
        </tbody>
        </table>
        <a href=' <c:url value="contact-form/${userId}"></c:url> ' class="btn btn-primary" role="button" >Add Contact</a>
	</div>    

	</body>
</html>



